namespace :dev do
  desc "Setup a environment of development"
  task setup: :environment do

    #puts "Reseting Database..."
    # %x(rails db:drop db:create db:migrate)

    p "Creating types of contacts..."

    kinds = %w(Family Commercial Known)
     kinds.each do |kind|
        Kind.create!(
          description: kind
        )
    end
    
    p "Types of Contacts created succefully!"

    ######################################

    p "Creating contacts..."

    100.times do |i|
        Contact.create!(
          name: Faker::Name.name,
          email: Faker::Internet.email,
          birthdate: Faker::Date.between(65.years.ago, 18.years.ago),
          kind: Kind.all.sample
        )
    end
    
    p "Contacts created succefully!"

    ######################

    p "Creating Phone of contacts..."

    Contact.all.each do |contact|
      Random.rand(5).times do |i|
        phone = Phone.create!(number:Faker::PhoneNumber.cell_phone)
        contact.phones << phone
        contact.save!
      end
    end

    p "Contacts phone created succefully!"

    ######################

    puts "Creating address of contacts..."

    Contact.all.each do |contact|
      Address.create(
        street: Faker::Address.street_address,
        city: Faker::Address.city,
        contact: contact
      )
    end

    puts "Contacts address created succefully!"

  end
end