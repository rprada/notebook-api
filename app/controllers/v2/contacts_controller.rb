module V2
  class ContactsController < ApplicationController

    #include ActionController::HttpAuthentication::Basic::ControllerMethods
    #http_basic_authenticate_with name: "rprada", password: "bnhjui89"

    #TOKEN = "bnhjui89"

    # include ActionController::HttpAuthentication::Token::ControllerMethods
    # before_action :authenticate
    #before_action :authenticate_user!

    before_action :set_contact, only: [:show, :update, :destroy]

    # GET /contacts
    def index
      @contacts = Contact.last(10)
      render json: @contacts, methods: :birthdate_br #methods:[ :hello, :i18n ]
      #render json: @contacts, methods: :author
      #render json: @contacts.map { |contact| contact.attributes.merge({ author: "Jackson" }) }
      #render json: @contacts, only: [:name, :email], root: true except: [:name, :email]
    end

    # GET /contacts/1
    def show
      render json: @contact, include: [:kind, :phones, :address] #, meta: { author: "Jackson Pires" }#.to_br
      #render json: @contact.attributes.merge({ author: "Jackson" })
    end

    # POST /contacts
    def create
      @contact = Contact.new(contact_params)

      if @contact.save
        render json: @contact, include: [:kind, :phones, :address], status: :created, location: @contact
      else
        render json: @contact.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /contacts/1
    def update
      if @contact.update(contact_params)
        render json: @contact, include: [:kind, :phones, :address]
      else
        render json: @contact.errors, status: :unprocessable_entity
      end
    end

    # DELETE /contacts/1
    def destroy
      @contact.destroy
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_contact
        @contact = Contact.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def contact_params
        # params.require(:contact).permit(:name, :email, :birthdate, :kind_id,
        #                             phones_attributes: [ :id, :number, :_destroy],
        #                             address_attributes: [ :id, :street, :city] )
        ActiveModelSerializers::Deserialization.jsonapi_parse(params)
      end

      # def authenticate
      #   authenticate_or_request_with_http_token do |token, options|
      #     # Compare the tokens in a time-constant manner, to mitigate
      #     # timing attacks.
      #     #ActiveSupport::SecurityUtils.secure_compare(token, TOKEN)

      #     hmac_secret = 'my$ecretK3y'
      #     JWT.decode token, hmac_secret, true, { :algorithm => 'HS256' }

      #   end
      # end
  end
end
